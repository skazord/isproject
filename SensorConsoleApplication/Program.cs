﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SensorNodeDll;
using System.Xml;
using System.Messaging;
using System.IO;

namespace SensorConsoleApplication {

    class Program {
        static string xmlFilePath;
        static string queuePath;
        static string alarmQueuePath;

        static int numSensor = 0;
        static XmlDocument xmlDoc;
        static XmlDocument configXml;
        static XmlElement sensorNode;

        static SensorNodeDll.SensorNodeDll sensor = new SensorNodeDll.SensorNodeDll();
        static void Main(string[] args) {
            // verify if config.xml exists, creates the file if not
            if (!File.Exists(AppDomain.CurrentDomain.BaseDirectory + "\\config.xml")) {
                Console.WriteLine("config.xml not found, generating default config file");
                createConfigFile();
            }
            loadSettingsFromFile();

            sensor.Initialize(parseSensorInfo, 1000);
        }

        private static void loadSettingsFromFile() {
            configXml = new XmlDocument();
            configXml.Load(AppDomain.CurrentDomain.BaseDirectory + "\\config.xml");
            xmlFilePath = configXml.DocumentElement.SelectSingleNode("/config/xmlfilepath").InnerText;
            queuePath = configXml.DocumentElement.SelectSingleNode("/config/queuepath").InnerText;
            alarmQueuePath = configXml.DocumentElement.SelectSingleNode("/config/alarmqueuepath").InnerText;
        }

        /*
        * Creates a default config file, stored at the root of the SensorConsoleApplication
        * Default XML file directory: PC Desktop
        * Default message queue name: xmlsensorqueue
        * Default alarm message queue name: xmlalarmqueue
        */
        private static void createConfigFile() {
            configXml = new XmlDocument();
            XmlElement configNode;

            configNode = configXml.CreateElement("config");
            configXml.AppendChild(configNode);

            XmlElement xmlFilePath = configXml.CreateElement("xmlfilepath");
            XmlElement queuePath = configXml.CreateElement("queuepath");
            XmlElement alarmQueuePath = configXml.CreateElement("alarmqueuepath");
            configNode.AppendChild(xmlFilePath);
            configNode.AppendChild(queuePath);
            configNode.AppendChild(alarmQueuePath);

            string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            XmlText xmlPathText = configXml.CreateTextNode(desktopPath + "\\sensordata.xml");
            xmlFilePath.AppendChild(xmlPathText);
            
            XmlText queuePathText = configXml.CreateTextNode(".\\Private$\\xmlsensorqueue"); //"FormatName:DIRECT=OS:.\\Private$\\xmlsensorqueue"
            queuePath.AppendChild(queuePathText);

            XmlText alarmQueuePathText = configXml.CreateTextNode(".\\Private$\\xmlalarmqueue"); //"FormatName:DIRECT=OS:.\\Private$\\xmlalarmqueue"
            alarmQueuePath.AppendChild(alarmQueuePathText);

            configXml.Save(AppDomain.CurrentDomain.BaseDirectory + "\\config.xml");
        }

        private static void parseSensorInfo(string info) {
            string date = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
            //Info format: 1;T;20.5
            Console.WriteLine(date + " - " + info);

            //parse info
            string[] infoParts = info.Split(';');
            string sensorType = infoParts[1];
            string sensorValue = infoParts[2];

            if (sensorType.Equals("T"))
                sensorType = "temperature";
            else {
                if (sensorType.Equals("P")) {
                    sensorType = "pressure";
                } else {
                    sensorType = "humidity";
                }
            }
            
            //(re-)create new, empty XML file
            if (numSensor == 0) {
                xmlDoc = new XmlDocument();
                sensorNode = xmlDoc.CreateElement("sensor");
                xmlDoc.AppendChild(sensorNode);
            }

            //transform info to XML
            XmlElement element = xmlDoc.CreateElement("sensorElement");
            sensorNode.AppendChild(element);

            XmlElement typeElement = xmlDoc.CreateElement("type");
            XmlElement valueElement = xmlDoc.CreateElement("value");
            XmlElement timeElement = xmlDoc.CreateElement("time");

            element.AppendChild(typeElement);
            element.AppendChild(valueElement);
            element.AppendChild(timeElement);
            
            XmlText typeText = xmlDoc.CreateTextNode(sensorType);
            XmlText valueText = xmlDoc.CreateTextNode(sensorValue);
            XmlText timeText = xmlDoc.CreateTextNode(date);
            typeElement.AppendChild(typeText);
            valueElement.AppendChild(valueText);
            timeElement.AppendChild(timeText);

            numSensor++;

            //send entire XML to MSMQ when number of elements exceeds 100
            //sends only as a single message
            if (numSensor > 4) {
                xmlDoc.Save(xmlFilePath);
                numSensor = 0;

                //Send info to communication hub
                MessageQueue queue = new MessageQueue(queuePath);
                MessageQueueTransaction mqTran = new MessageQueueTransaction();
                mqTran.Begin();
                try {
                    queue.Send(xmlDoc.InnerXml, "SensorQueueXML", mqTran);
                    Console.WriteLine("XML message pushed to Queue.");
                    mqTran.Commit();
                } catch (Exception Exception) {
                    Console.WriteLine(Exception);
                    mqTran.Abort();
                }
                queue.Close();

                MessageQueue queueAlarm = new MessageQueue(alarmQueuePath);
                MessageQueueTransaction mqTranAlarm = new MessageQueueTransaction();
                mqTranAlarm.Begin();
                try {
                    queueAlarm.Send(xmlDoc.InnerXml, "AlarmQueueXML", mqTranAlarm);
                    Console.WriteLine("XML message pushed to Alarm Queue.");
                    mqTranAlarm.Commit();
                } catch (Exception Exception) {
                    Console.WriteLine(Exception);
                    mqTranAlarm.Abort();
                }
                queueAlarm.Close();

            }
        }


        private static void stopSensor() {
            sensor.Stop();
        }
    }
}
