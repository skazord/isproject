﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Messaging;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace AlarmingSystemApplication {
    static class Program {
        static XmlDocument configXml;
        static string queuePath;

        public static Form1 MyForm { get; private set; }

        [STAThread]
        static void Main() {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            MyForm = new Form1();
            Application.Run(MyForm);
        }

        private static void loadSettingsFromFile() {
            configXml = new XmlDocument();
            configXml.Load(AppDomain.CurrentDomain.BaseDirectory + "\\config.xml");
            queuePath = configXml.DocumentElement.SelectSingleNode("/config/queuepath").InnerText;
        }

        private static void createConfigFile() {
            configXml = new XmlDocument();
            XmlElement configNode;

            configNode = configXml.CreateElement("config");
            configXml.AppendChild(configNode);
            
            XmlElement queuePath = configXml.CreateElement("queuepath");
            configNode.AppendChild(queuePath);

            XmlText queuePathText = configXml.CreateTextNode(".\\Private$\\xmlalarmqueue"); //"FormatName:.\\Private$\\xmlalarmqueue"
            queuePath.AppendChild(queuePathText);

            configXml.Save(AppDomain.CurrentDomain.BaseDirectory + "\\config.xml");
        }

        public static void startAlarmSystem(string minTempVal, string maxTempVal, string minPressVal, string maxPressVal, string minHumVal, string maxHumVal) {
            double minTemp = convertToDouble(minTempVal);
            double maxTemp = convertToDouble(maxTempVal);
            double minPress = convertToDouble(minPressVal);
            double maxPress = convertToDouble(maxPressVal);
            double minHum = convertToDouble(minHumVal);
            double maxHum = convertToDouble(maxHumVal);
            
            if (!File.Exists(AppDomain.CurrentDomain.BaseDirectory + "\\config.xml")) {
                createConfigFile();
            }
            loadSettingsFromFile();

            //obtain sensor info from message and parse it
            if (MessageQueue.Exists(queuePath)) {
                try {
                    MessageQueue.Delete(queuePath);
                } catch (MessageQueueException e) {
                    Console.WriteLine(e.Message);
                }                
            }
            MessageQueue queueAlarm = MessageQueue.Create(queuePath, true);
            
            try {
                queueAlarm.Formatter = new XmlMessageFormatter(new Type[] { typeof(String) });
                String str = (String)queueAlarm.Receive().Body;

                XmlDocument alarmXml = new XmlDocument();
                alarmXml.LoadXml(str);

                XmlElement root = alarmXml.DocumentElement;
                XmlNodeList nodes = root.SelectNodes("sensorElement");
                foreach (XmlNode node in nodes) {
                    string type = (string)node["type"].InnerText;
                    string valstr = node["value"].InnerText;
                    valstr = valstr.Replace(".", ",");
                    double value = Double.Parse(valstr);
                    string date = node["time"].InnerText;

                    emitWarningsToForm(minTemp, maxTemp, minPress, maxPress, minHum, maxHum, type, value, date);
                }

            } catch (MessageQueueException e) {
                Console.WriteLine("Queue error " + e.Message);
            }
            Program.startAlarmSystem(minTempVal, maxTempVal, minPressVal, maxPressVal, minHumVal, maxHumVal);
        }

        private static void emitWarningsToForm(double minTemp, double maxTemp, double minPress, double maxPress, double minHum, double maxHum, 
            string type, double value, string date) {

            if (type.Equals("temperature")) {
                if (minTemp != -1) {
                    if (value < minTemp) {
                        MyForm.Invoke((MethodInvoker)delegate(){
                            MyForm.AddToListBox("[" + date + "] " + "Recorded " + value + " ºC, which is lower than the minimum allowed temperature");
                        });
                    }
                }

                if (maxTemp != -1) {
                    if (value > maxTemp) {
                        MyForm.Invoke((MethodInvoker)delegate () {
                            MyForm.AddToListBox("[" + date + "] " + "Recorded " + value + " ºC, which is higher than the maximum allowed temperature");
                        });
                    }
                }
            }

            if (type.Equals("pressure")){
                if (minPress != -1) {
                    if (value < minPress) {
                        MyForm.Invoke((MethodInvoker)delegate () {
                            MyForm.AddToListBox("[" + date + "] " + "Recorded " + value + " PSI, which is lower than the minimum allowed pressure");
                        });
                    }
                }

                if (maxPress != -1) {
                    if (value > maxPress) {
                        MyForm.Invoke((MethodInvoker)delegate () {
                            MyForm.AddToListBox("[" + date + "] " + "Recorded " + value + " PSI, which is higher than the maximum allowed pressure");
                        });
                    }
                }
            }

            if (type.Equals("humidity")) {
                if (minHum != -1) {
                    if (value < minHum) {
                        MyForm.Invoke((MethodInvoker)delegate () {
                            MyForm.AddToListBox("[" + date + "] " + "Recorded " + value + "% humidity, which is lower than the minimum allowed humidity");
                        });
                    }
                }

                if (maxHum != -1) {
                    if (value > maxHum) {
                        MyForm.Invoke((MethodInvoker)delegate () {
                            MyForm.AddToListBox("[" + date + "] " + "Recorded " + value + "% humidity, which is higher than the maximum allowed humidity");
                        });
                    }
                }
            }
            
        }

        private static double convertToDouble(string val) {
            double valDouble;
            if (!val.Equals("-")) {
                valDouble = Convert.ToDouble(val);
                return valDouble;
            }
            return -1;            
        }
    }
}
