﻿namespace AlarmingSystemApplication {
    partial class Form1 {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnStartAlarm = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cBoxTempMin = new System.Windows.Forms.ComboBox();
            this.cBoxTempMax = new System.Windows.Forms.ComboBox();
            this.cBoxPressMin = new System.Windows.Forms.ComboBox();
            this.cBoxPressMax = new System.Windows.Forms.ComboBox();
            this.cBoxHumMin = new System.Windows.Forms.ComboBox();
            this.cBoxHumMax = new System.Windows.Forms.ComboBox();
            this.listBoxWarnings = new System.Windows.Forms.ListBox();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnStartAlarm
            // 
            this.btnStartAlarm.Location = new System.Drawing.Point(86, 149);
            this.btnStartAlarm.Name = "btnStartAlarm";
            this.btnStartAlarm.Size = new System.Drawing.Size(75, 23);
            this.btnStartAlarm.TabIndex = 0;
            this.btnStartAlarm.Text = "Start Alarm";
            this.btnStartAlarm.UseVisualStyleBackColor = true;
            this.btnStartAlarm.Click += new System.EventHandler(this.btnStartAlarm_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Temperature";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Pressure";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 107);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Humidity";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(120, 6);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(24, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Min";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(196, 6);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(27, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Max";
            // 
            // cBoxTempMin
            // 
            this.cBoxTempMin.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBoxTempMin.FormattingEnabled = true;
            this.cBoxTempMin.Location = new System.Drawing.Point(99, 31);
            this.cBoxTempMin.Name = "cBoxTempMin";
            this.cBoxTempMin.Size = new System.Drawing.Size(62, 21);
            this.cBoxTempMin.TabIndex = 6;
            this.cBoxTempMin.SelectedIndexChanged += new System.EventHandler(this.cBoxTempMin_SelectedIndexChanged);
            // 
            // cBoxTempMax
            // 
            this.cBoxTempMax.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBoxTempMax.FormattingEnabled = true;
            this.cBoxTempMax.Location = new System.Drawing.Point(177, 31);
            this.cBoxTempMax.Name = "cBoxTempMax";
            this.cBoxTempMax.Size = new System.Drawing.Size(62, 21);
            this.cBoxTempMax.TabIndex = 7;
            this.cBoxTempMax.SelectedIndexChanged += new System.EventHandler(this.cBoxTempMax_SelectedIndexChanged);
            // 
            // cBoxPressMin
            // 
            this.cBoxPressMin.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBoxPressMin.FormattingEnabled = true;
            this.cBoxPressMin.Location = new System.Drawing.Point(99, 67);
            this.cBoxPressMin.Name = "cBoxPressMin";
            this.cBoxPressMin.Size = new System.Drawing.Size(62, 21);
            this.cBoxPressMin.TabIndex = 8;
            this.cBoxPressMin.SelectedIndexChanged += new System.EventHandler(this.cBoxPressMin_SelectedIndexChanged);
            // 
            // cBoxPressMax
            // 
            this.cBoxPressMax.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBoxPressMax.FormattingEnabled = true;
            this.cBoxPressMax.Location = new System.Drawing.Point(177, 67);
            this.cBoxPressMax.Name = "cBoxPressMax";
            this.cBoxPressMax.Size = new System.Drawing.Size(62, 21);
            this.cBoxPressMax.TabIndex = 9;
            this.cBoxPressMax.SelectedIndexChanged += new System.EventHandler(this.cBoxPressMax_SelectedIndexChanged);
            // 
            // cBoxHumMin
            // 
            this.cBoxHumMin.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBoxHumMin.FormattingEnabled = true;
            this.cBoxHumMin.Location = new System.Drawing.Point(99, 104);
            this.cBoxHumMin.Name = "cBoxHumMin";
            this.cBoxHumMin.Size = new System.Drawing.Size(62, 21);
            this.cBoxHumMin.TabIndex = 10;
            this.cBoxHumMin.SelectedIndexChanged += new System.EventHandler(this.cBoxHumMin_SelectedIndexChanged);
            // 
            // cBoxHumMax
            // 
            this.cBoxHumMax.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBoxHumMax.FormattingEnabled = true;
            this.cBoxHumMax.Location = new System.Drawing.Point(177, 104);
            this.cBoxHumMax.Name = "cBoxHumMax";
            this.cBoxHumMax.Size = new System.Drawing.Size(62, 21);
            this.cBoxHumMax.TabIndex = 11;
            this.cBoxHumMax.SelectedIndexChanged += new System.EventHandler(this.cBoxHumMax_SelectedIndexChanged);
            // 
            // listBoxWarnings
            // 
            this.listBoxWarnings.FormattingEnabled = true;
            this.listBoxWarnings.Location = new System.Drawing.Point(257, 25);
            this.listBoxWarnings.Name = "listBoxWarnings";
            this.listBoxWarnings.Size = new System.Drawing.Size(557, 147);
            this.listBoxWarnings.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(254, 6);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Warnings";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(826, 184);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.listBoxWarnings);
            this.Controls.Add(this.cBoxHumMax);
            this.Controls.Add(this.cBoxHumMin);
            this.Controls.Add(this.cBoxPressMax);
            this.Controls.Add(this.cBoxPressMin);
            this.Controls.Add(this.cBoxTempMax);
            this.Controls.Add(this.cBoxTempMin);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnStartAlarm);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnStartAlarm;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cBoxTempMin;
        private System.Windows.Forms.ComboBox cBoxTempMax;
        private System.Windows.Forms.ComboBox cBoxPressMin;
        private System.Windows.Forms.ComboBox cBoxPressMax;
        private System.Windows.Forms.ComboBox cBoxHumMin;
        private System.Windows.Forms.ComboBox cBoxHumMax;
        private System.Windows.Forms.ListBox listBoxWarnings;
        private System.Windows.Forms.Label label6;
    }
}

