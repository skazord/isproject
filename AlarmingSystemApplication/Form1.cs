﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace AlarmingSystemApplication {
    public partial class Form1 : Form {
        XmlDocument settingsXml;
        public Form1() {
            InitializeComponent();
            fillComboBoxes();
            initializeXml();
        }
        
        private void fillComboBoxes() {
            cBoxHumMax.Items.Add("-");
            cBoxHumMin.Items.Add("-");
            cBoxPressMax.Items.Add("-");
            cBoxPressMin.Items.Add("-");
            cBoxTempMax.Items.Add("-");
            cBoxTempMin.Items.Add("-");

            for (int i=18; i<29; i++) {
                cBoxTempMin.Items.Add(i);
                cBoxTempMax.Items.Add(i);
            }
            for(double k=0; k<1; k += 0.1) {
                cBoxPressMin.Items.Add(30 + k);
                cBoxPressMax.Items.Add(30 + k);
            }
            for(double n=70; n<85; n++) {
                cBoxHumMin.Items.Add(n);
                cBoxHumMax.Items.Add(n);
            }
        }

        private void btnStartAlarm_Click(object sender, EventArgs e) {
            string minTempVal = (string)cBoxTempMin.SelectedItem.ToString();
            string maxTempVal = (string)cBoxTempMax.SelectedItem.ToString();
            string minPressVal = (string)cBoxPressMin.SelectedItem.ToString();
            string maxPressVal = (string)cBoxPressMax.SelectedItem.ToString();
            string minHumVal = (string)cBoxHumMin.SelectedItem.ToString();
            string maxHumVal = (string)cBoxHumMax.SelectedItem.ToString();

            listBoxWarnings.Items.Add("Started listening for messages...");
            Task.Factory.StartNew(() => Program.startAlarmSystem(minTempVal, maxTempVal, minPressVal, maxPressVal, minHumVal, maxHumVal));
        }

        public void AddToListBox(string str) {
            listBoxWarnings.Items.Add(str);
            listBoxWarnings.TopIndex = listBoxWarnings.Items.Count - 1;
        }

        private void initializeXml() {
            if (!File.Exists(AppDomain.CurrentDomain.BaseDirectory + "\\settings.xml")){
                createSettingsXml();
            }

            settingsXml = new XmlDocument();
            settingsXml.Load(AppDomain.CurrentDomain.BaseDirectory + "\\settings.xml");

            cBoxHumMax.SelectedIndex = cBoxHumMax.FindStringExact(settingsXml.DocumentElement.SelectSingleNode("/settings/elemMaxHum").InnerText);
            cBoxHumMin.SelectedIndex = cBoxHumMin.FindStringExact(settingsXml.DocumentElement.SelectSingleNode("/settings/elemMinHum").InnerText);
            cBoxTempMax.SelectedIndex = cBoxTempMax.FindStringExact(settingsXml.DocumentElement.SelectSingleNode("/settings/elemMaxTemp").InnerText);
            cBoxTempMin.SelectedIndex = cBoxTempMin.FindStringExact(settingsXml.DocumentElement.SelectSingleNode("/settings/elemMinTemp").InnerText);
            cBoxPressMax.SelectedIndex = cBoxPressMax.FindStringExact(settingsXml.DocumentElement.SelectSingleNode("/settings/elemMaxPress").InnerText);
            cBoxPressMin.SelectedIndex = cBoxPressMin.FindStringExact(settingsXml.DocumentElement.SelectSingleNode("/settings/elemMinPress").InnerText);
        }

        private void createSettingsXml() {
            settingsXml = new XmlDocument();
            XmlElement settingsNode;

            settingsNode = settingsXml.CreateElement("settings");
            settingsXml.AppendChild(settingsNode);

            XmlElement elemMinTemp = settingsXml.CreateElement("elemMinTemp");
            XmlElement elemMaxTemp = settingsXml.CreateElement("elemMaxTemp");
            XmlElement elemMinPress = settingsXml.CreateElement("elemMinPress");
            XmlElement elemMaxPress = settingsXml.CreateElement("elemMaxPress");
            XmlElement elemMinHum = settingsXml.CreateElement("elemMinHum");
            XmlElement elemMaxHum = settingsXml.CreateElement("elemMaxHum");

            settingsNode.AppendChild(elemMinTemp);
            settingsNode.AppendChild(elemMaxTemp);
            settingsNode.AppendChild(elemMinPress);
            settingsNode.AppendChild(elemMaxPress);
            settingsNode.AppendChild(elemMinHum);
            settingsNode.AppendChild(elemMaxHum);

            XmlText minTempText = settingsXml.CreateTextNode("-");
            XmlText maxTempText = settingsXml.CreateTextNode("-");
            XmlText minPressText = settingsXml.CreateTextNode("-");
            XmlText maxPressText = settingsXml.CreateTextNode("-");
            XmlText minHumText = settingsXml.CreateTextNode("-");
            XmlText maxHumText = settingsXml.CreateTextNode("-");

            elemMinTemp.AppendChild(minTempText);
            elemMaxTemp.AppendChild(maxTempText);
            elemMinPress.AppendChild(minPressText);
            elemMaxPress.AppendChild(maxPressText);
            elemMinHum.AppendChild(minHumText);
            elemMaxHum.AppendChild(maxHumText);

            settingsXml.Save(AppDomain.CurrentDomain.BaseDirectory + "\\settings.xml");
        }

        private void cBoxTempMin_SelectedIndexChanged(object sender, EventArgs e) {
            if (!cBoxTempMax.SelectedItem.Equals("-") && !cBoxTempMin.SelectedItem.Equals("-")) {
                double valTempMax = Double.Parse((string)cBoxTempMax.SelectedItem);
                double valTempMin = Double.Parse((string)cBoxTempMin.SelectedItem);

                if(valTempMin > valTempMax) {

                }
                if(valTempMin == valTempMax) {

                }

            }

            settingsXml = new XmlDocument();
            settingsXml.Load(AppDomain.CurrentDomain.BaseDirectory + "\\settings.xml");
            settingsXml.DocumentElement.SelectSingleNode("/settings/elemMinTemp").InnerText = (string)cBoxTempMin.SelectedItem.ToString();
            settingsXml.Save(AppDomain.CurrentDomain.BaseDirectory + "\\settings.xml");
        }

        private void cBoxTempMax_SelectedIndexChanged(object sender, EventArgs e) {
            settingsXml = new XmlDocument();
            settingsXml.Load(AppDomain.CurrentDomain.BaseDirectory + "\\settings.xml");
            settingsXml.DocumentElement.SelectSingleNode("/settings/elemMaxTemp").InnerText = (string)cBoxTempMax.SelectedItem.ToString();
            settingsXml.Save(AppDomain.CurrentDomain.BaseDirectory + "\\settings.xml");
        }

        private void cBoxPressMin_SelectedIndexChanged(object sender, EventArgs e) {
            settingsXml = new XmlDocument();
            settingsXml.Load(AppDomain.CurrentDomain.BaseDirectory + "\\settings.xml");
            settingsXml.DocumentElement.SelectSingleNode("/settings/elemMinPress").InnerText = (string)cBoxPressMin.SelectedItem.ToString();
            settingsXml.Save(AppDomain.CurrentDomain.BaseDirectory + "\\settings.xml");
        }

        private void cBoxPressMax_SelectedIndexChanged(object sender, EventArgs e) {
            settingsXml = new XmlDocument();
            settingsXml.Load(AppDomain.CurrentDomain.BaseDirectory + "\\settings.xml");
            settingsXml.DocumentElement.SelectSingleNode("/settings/elemMaxPress").InnerText = (string)cBoxPressMax.SelectedItem.ToString();
            settingsXml.Save(AppDomain.CurrentDomain.BaseDirectory + "\\settings.xml");
        }

        private void cBoxHumMin_SelectedIndexChanged(object sender, EventArgs e) {
            settingsXml = new XmlDocument();
            settingsXml.Load(AppDomain.CurrentDomain.BaseDirectory + "\\settings.xml");
            settingsXml.DocumentElement.SelectSingleNode("/settings/elemMinHum").InnerText = (string)cBoxHumMin.SelectedItem.ToString();
            settingsXml.Save(AppDomain.CurrentDomain.BaseDirectory + "\\settings.xml");
        }

        private void cBoxHumMax_SelectedIndexChanged(object sender, EventArgs e) {
            settingsXml = new XmlDocument();
            settingsXml.Load(AppDomain.CurrentDomain.BaseDirectory + "\\settings.xml");
            settingsXml.DocumentElement.SelectSingleNode("/settings/elemMaxHum").InnerText = (string)cBoxHumMax.SelectedItem.ToString();
            settingsXml.Save(AppDomain.CurrentDomain.BaseDirectory + "\\settings.xml");
        }
    }
}
